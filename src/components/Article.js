import React, { Component } from 'react';

/*
* This is the component which holds a News Article (title and description)
*/
class Article extends Component {
  render() {
        return (
          <div className="newsList">
            <b>{this.props.title}</b>
            <br/>{this.props.desc}<hr/>
          </div>
        );
  }
}

export default Article;
