import React, { Component } from 'react';
import Article from './Article.js';

/*
* This is the component which holds a list of News Articles
* Multiple Article components are loaded
*/
class NewsList extends Component {

    constructor(props){
      super(props);
      this.state={
        data:[],
        agency: this.props.agencyId,
        agencyName: this.props.agencyName
      }
      var component = this;
      this.props.eventEmitter.addListener("updateNewsAgency", function({id,agency}){
        component.setState({
          data:[],
          agency: id,
          agencyName: agency
        })
        component.fetchNews(id,agency);
      });
    }

    fetchNews(agencyId, agencyName) {
      let URL = 'https://newsapi.org/v1/articles?source=' + agencyId + '&apiKey='+ this.props.api_key
      var component = this;

      fetch(URL)
        .then((response) => {
          return response.json()
        })
        .then((json) => {
          component.setState({
            data: json.articles,
            agency: agencyId,
            agencyName: agencyName
          })

        })
        .catch((ex) => {
          console.log('parsing failed', ex)
        });
    }

    componentDidMount() {
      this.fetchNews(this.state.agency, this.state.agencyName);
    }

    render() {
      // The important code which identifies the count of news entries and article nodes created
      var articleNodes = this.state.data.map(function(article){
          return (
            <Article title={article.title} desc={article.description} url={article.url}>
            </Article>
          )
      });
      return (
        <div className="main-content">
          <h2>Showing Latest News from {this.state.agencyName}</h2>
          {articleNodes}
        </div>
      );
  }
}

export default NewsList;
