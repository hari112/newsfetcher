import React, { Component } from 'react';

class NewsSource extends Component {
  render() {
        return (
          < div className = "nav-item"
            onClick = {
                (event) => {
                  this.props.eventEmitter.emit("updateNewsAgency", {
                    id: this.props.id,
                    agency: this.props.agency
                  })
                }
              } > { this.props.agency } </div>
        );
  }
}

export default NewsSource;
