import React, { Component } from 'react';
import NewsSource from './NewsSource.js';

const BASE_URL_FOR_NEWS_SOURCE = 'https://newsapi.org/v1/sources?language=en&apiKey='

class NewsList extends Component {

  constructor(props){
      super(props);
      this.state={
        data:[]
      }
  }

  componentDidMount() {
      let URL = BASE_URL_FOR_NEWS_SOURCE + this.props.api_key
      var component = this;

      fetch(URL)
        .then((response) => {
          return response.json()
        })
        .then((json) => {
          component.setState({
            data: json.sources
          })
        })
        .catch((ex) => {
          console.log('parsing failed', ex)
        });
  }

  render() {
    let eventEmitter = this.props.eventEmitter;
    // The agency nodes created here.
    var sourceNodes = this.state.data.map(function(source){
          return (
            <NewsSource agency={source.name} id={source.id}  eventEmitter={eventEmitter}  />
          )
        });
        return (
          <div className="newsSourceList">
            {sourceNodes}
          </div>
        );
  }
}

export default NewsList;
