import React, { Component } from 'react';
import './App.css';
import NewsSourceList from './components/NewsSourceList.js';
import NewsList from './components/NewsList.js';
import { EventEmitter } from 'events'

const DEFAULT_NEWS_AGENCY_ID = 'abc-news-au'
const DEFAULT_NEWS_AGENCY_NAME = 'ABC News (AU)'
const NEWSAPI_ORG_API_KEY = 'f9892d6a466045baafa4e1b388745e2b'

/*
* This is the main component where all the sub components are loaded.
* Author : Hari Nair
*/
class App extends Component {

  componentWillMount() {
    // Event emitter is used to broadcast news agency nav event to NewsList component
    this.eventEmitter = new EventEmitter();
  }

  render() {
    return (
      <div className="app">
        <div className="app-header">
          <h1><center>News Fetcher App</center></h1>
        </div>
        <div className="app-wrapper">
          <div className="app-nav" ><NewsSourceList eventEmitter={this.eventEmitter} api_key={NEWSAPI_ORG_API_KEY} />
          </div>
          <div className="app-main">
          <NewsList eventEmitter={this.eventEmitter} agencyName={DEFAULT_NEWS_AGENCY_NAME} api_key={NEWSAPI_ORG_API_KEY}  agencyId={DEFAULT_NEWS_AGENCY_ID} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
