# New Fetcher App #
News Fetcher App is a serverless single page web application which loads news from different news agencies from internet via newsapi.org’s API gateway. Once the App is loaded in browser the browser connects to the newsapi.org’s API and shows the list of News Sources in the navigation bar. By selecting any news source the news from that particular agency is displayed.

## Prerequisites ##

Internet must be available for running the App.
Node is required to be installed. 
Node JS version 6 or above - https://nodejs.org/en/download/.

## Installing ##

After extracting the zip folder . In CLI please execute 
npm install


All the required dependent node_modules will be downloaded.
To run the application by running following command
*npm start*

To check if installation is proper by reading the console log.

## Running the App ##

After installation the app will be available at localhost:3000

## Features of App ##

On launching the App, the available news agencies will get listed on the navigation bar on left.
Recent News will be shown. (Initially the news from ‘ABC news’ is shown )
The user can navigate to a different news source by clicking the corresponding News Agency and as soon as they click the latest news from that agency will be displayed.
Screenshot of the App

## Known Issues ##
Issue:
The Active/Selected Nav Item is not highlighted

Remedy:
While rendering the Navigator the currently selected News Source(Active) must be checked against each Nav Item and css class for active to be dynamically assigned.

Issue:
When the App loads The default news shown is from ABC News.

Remedy:
The default agency must be the 1st from the list fetched. 

Issue:
If internet is slow there could be a delay when loading news. A throbber missing.

Remedy:
Throbber to be shown when API request begins and must hide when result fetched.

Issue:
Error handling is not there. Just console log.

Remedy:
An option to retry when an error occur and show it to user in App.

Issue:
Test cases missing

Remedy:
JSON data from newsapi.org must be mocked and tested for parse errors.

## Technical Details ##
The App is build using React JS library. There is a main Component called App and four Sub Components to load the news source and the news itself.
Featured used from React

* Components are isolated separate source code file for each.

* Event Broadcasting and subscription used.

* Use of Properties (props), JSX, Closures etc. 

The newsapi.org API documentation available at https://newsapi.org/#documentation
To use the gateway API Key was generated and hardcoded in the App

## Versioning ##
Version 1.0.0

## Author ##
Hari Nair

## License ##
No copyrights